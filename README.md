TGVTrack v2
===========

The purpose of this codebase is to pull some trafic informations on a daily
basis out of the Navitia API exposed by SNCF (thus requiring registration at
https://www.digital.sncf.com/startup/api) for archiving.

Environment setup
=================
Make sure you build a Python3 virtualenv and populate it:
```
$ virtualenv -p python3 venv/
$ source venv/bin/activate
$ pip install -r requirements.txt
```

Configuration
=============
Create a `tgvretards.ini` configuration file from `tgvretards.ini.dist`.
Fields are:
 - `url`: Navitia API endpoint to use
 - `token`: Your Navitia API username / token

Then you can build sections as this:
```
[SECTION-NAME]
stop1 = stop_area
stop2 = stop_area
extra = stop_area_1,stop_area_2,...
```

Which stands for:
 - `SECTION-NAME`: a valid directory name describing the line to fetch data
 - `stop1` and `stop2`: proper stop_area for your stations, from/to.
   Find then using the Navitia Playground: http://canaltp.github.io/navitia-playground/index.html
 - `extra`: a comma-separated list of stop_area that we accept connections.
   This is here to control the train stations where you accept connections to
   happen, otherwise the Navitia API can give valid but results that would not
   be convenient on a daily-basis (example: "Massy TGV" to "Tours" would yield
   results via "Le Mans", "Angers Saint-Laud" or even "Nantes" unless you add
   an extra with stop_area for Saint-Pierre-des-Corps).

Usage
=====
Just run like that, it should automatically start by fetching the last three
weeks of data:
```
$ python3 tgvretards.py
```

Further run should only fetch new data starting from the latest ones to those
from yesterday.
