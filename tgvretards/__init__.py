import os
import requests
import configparser
import itertools
import datetime

config = configparser.ConfigParser()

config['DEFAULT'] = {
    'url': 'https://api.navitia.io/v1',
    'user': "token"
}

config['Tours-Paris'] = {
    'stop1': 'stop_area:OCE:SA:87571000',
    'stop2': 'stop_area:OCE:SA:87391003',
    'extra': 'stop_area:OCE:SA:87571240'
}

def get_config():
    CONFIG_FILE = 'tgvretards.ini'
    CONFIG_DIST = 'tgvretards.ini.dist'

    with open(CONFIG_DIST, 'w') as cd:
        config.write(cd)

    if not os.path.isfile(CONFIG_FILE):
        raise Exception("Missing configuration file")

    _c = configparser.ConfigParser()
    _c.read(CONFIG_FILE)

    print(_c.sections())

    return _c

def create_allowed_ids(section=None):
    return { 'allowed_id[]': section.get('extra', '').split(',') + [ section['stop1'], section['stop2'] ] }

def query(API=None, section=None, date=None):
    rs = []

    assert len(date) == len('20180824')
    query_date = datetime.datetime.strptime(date, '%Y%m%d').replace(hour=4, minute=0, second=0)
    query_ts   = query_date.strftime('%Y%m%dT%H%M%S')

    allowed_ids = create_allowed_ids(section)

    journeys = '%(API)s/coverage/sncf/journeys' % {
        'API': API[0],
    }

    dates = {
        'datetime':            query_ts,
        'datetime_represents': 'departure',
        'timeframe_duration':  79200
    }

    payload = {
        'count':            128,
        'max_nb_transfers': 1,
        'data_freshness':   'realtime'
    }

    payload.update(allowed_ids)
    payload.update(dates)

    for (_from, _to) in itertools.permutations([ section['stop1'], section['stop2'] ]):
        ft = {
            'from': _from,
            'to':   _to
        }
        payload.update(ft)

        r = requests.get(journeys, auth=(API[1], ''), params=payload)
        assert r.status_code == 200 or r.status_code == 404

        if r.status_code == 404:
            print('missing data (too late?) for {}'.format(date))

        rs.append(r.text)

    return rs

def ensure_dir(dirname=None):
    if not os.path.isdir(dirname):
        os.makedirs(dirname)

def get_missing_dates(dirname=None):
    if not os.path.isdir(dirname):
        raise BaseException('Inexistent section directory')

    today = datetime.datetime.now().replace(hour=4, minute=0, second=0, microsecond=0)

    def get_starting_date():
        files = list(filter(lambda x: not os.path.isfile(x) and x.endswith('.json'), os.listdir(dirname)))
        if len(files) == 0:
            return today - datetime.timedelta(days=21)

        dates = list(map(lambda y: datetime.datetime.strptime('%s' % (y.replace('.json', '')), '%Y%m%d').replace(hour=4, minute=0, second=0), files))
        assert len(dates) > 0

        dates.sort()
        return dates[-1] + datetime.timedelta(days=1)

    first_date = get_starting_date()
    delta = today - first_date
    return list(map(lambda x: first_date + datetime.timedelta(days=x), range(delta.days)))

def get_all_infos():
    c = get_config()

    ensure_dir('data')

    user = os.getenv("NAVITIA_API_USER", None)
    if user is None:
        user = c['DEFAULT']['user']

    API = (c['DEFAULT']['url'], user)

    for sec in c.sections():
        print('Section:', sec)
        ensure_dir('data/%s' % sec)
        for missing in get_missing_dates('data/%s' % sec):
            d = missing.strftime('%Y%m%d')
            print('\tDate:', d)
            jsons = query(API, c[sec], d)
            with open('data/%s/%s.json' % (sec, d), 'w') as js:
                js.write('\n'.join(jsons))
                js.write('\n')
